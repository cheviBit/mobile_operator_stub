package ru.iitdgroup.rshb.stub;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sentInboundMessage")
public class InboundRestController {

	private final Logger logger = Logger.getLogger(InboundRestController.class);

	@Autowired
	private MobileOperatorStub mobileOperatorStub;

	@RequestMapping(method = RequestMethod.GET, value = "/{phone}")
	public String sentInboundMessage(@PathVariable String phone) {
		try {
			boolean sendInbound = mobileOperatorStub.sendInbound(phone);
			if (sendInbound) {
				return "Sent";
			} else {
				return "phone not found";
			}
		} catch (Exception e) {
			logger.error("e", e);
			return "Can't send " + e.getMessage();
		}
	}
}
