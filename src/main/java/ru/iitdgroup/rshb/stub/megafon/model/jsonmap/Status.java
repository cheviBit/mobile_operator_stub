package ru.iitdgroup.rshb.stub.megafon.model.jsonmap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "name", "code" })
public class Status {
	@JsonProperty("name")
	private String name;
	@JsonProperty("code")
	private String code;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Status [name=" + name + ", code=" + code + "]";
	}
}
