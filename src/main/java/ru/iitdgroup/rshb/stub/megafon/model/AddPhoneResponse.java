package ru.iitdgroup.rshb.stub.megafon.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddPhoneResponse {

	@JsonProperty("result")
	private ResultImsi result;

	@JsonProperty("result")
	public ResultImsi getResult() {
		return result;
	}

	@JsonProperty("result")
	public void setResult(ResultImsi result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "AddPhoneResponse [result=" + result + "]";
	}

}
