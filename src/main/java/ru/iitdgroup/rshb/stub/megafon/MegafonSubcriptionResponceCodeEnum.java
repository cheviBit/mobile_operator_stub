package ru.iitdgroup.rshb.stub.megafon;

public enum MegafonSubcriptionResponceCodeEnum {

	OK("1", "ok", "Запрос выполнен успешно"), //
	WRONG_FORMAT_ERROR("101", "wrong_format_error", "При не правильном составление запроса в нем могут присутствовать недокументированные поля или отсутствовать обязательные поля, значения которых должно быть возвращено в ответе для дальнейшего анализа"), //
	WRONG_MSISDN("102", "wrong_msisdn", "Формат msisdn указан верно, но данный msisdn не принадлежит оператору МегаФон"), //
	ALREADY_SUBSCRIBED("103", "already_subscribed", "Данный msisdn уже подписан на сбор событий"), //
	UNKNOWN_MSISDN("105", "unknown_msisdn", "Ошибка. msisdn не подписан"), //
	UNKNOWN_PARAMS("106", "unknown_params", "Ошибка при запросе. Возникает, когда для запрашиваемого msisdn нет значений в полях, для которых разрешен просмотр"), //
	WRONG_SOURCE_IP_ERROR("108", "wrong_source_ip_error", "IP отсутствует в списке разрешенных"), //
	NOT_ALLOWED_ERROR("109", "not_allowed_error", "Ошибка. Возникает в случае, когда запрашиваемое действие запрещено"), //
	AGREEMENT_NOT_FOUND("110", "agreement_not_found", "Ошибка. Возникает при попытке добавить абонента, согласие которого не получено"), //
	CLIENT_DISABLED("111", "client_disabled", "Ошибка. Возникает в случае, когда клиент находится в статусе \"Заблокированный\""), //
	NO_PARAM_SUBSCRIPTION("112", "no_param_subscription", "Ошибка. Возникает в случае, когда клиент запрашивает информацию по определенному идентификатору абонента, не имея подписки на него"), //
	API_VERSION_RESTRICTED("113", "api_version_restricted", "Ошибка при запросе. Возникает когда клиент обращается к недоступной ему версии API"), //
	;
	
	private String code;	
	private String name;
	private String comment;

	private MegafonSubcriptionResponceCodeEnum(String code, String name, String comment) {
		this.code = code;
		this.name = name;
		this.comment = comment;
	}

	public String getCode() {
		return code;
	}	

	public String getName() {
		return name;
	}

	public String getComment() {
		return comment;
	}
	
	public static MegafonSubcriptionResponceCodeEnum getCode(String responceStatus) {
		for (MegafonSubcriptionResponceCodeEnum codeEnum : values()) {
			if (codeEnum.getCode().equals(responceStatus)) {
				return codeEnum;
			}
		}
		return null;
	}

}
