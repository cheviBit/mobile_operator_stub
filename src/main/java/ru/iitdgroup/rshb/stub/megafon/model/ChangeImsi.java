package ru.iitdgroup.rshb.stub.megafon.model;

import java.util.ArrayList;
import java.util.List;

import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.ResultChange;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "result", "time" })
public class ChangeImsi extends InboundMegafon {

	@JsonProperty("result")
	private List<ResultChange> result = new ArrayList<ResultChange>();
	@JsonProperty("time")
	public String time;

	@JsonProperty("result")
	public List<ResultChange> getResult() {
		return result;
	}

	@JsonProperty("result")
	public void setResult(List<ResultChange> result) {
		this.result = result;
	}

	@JsonProperty("time")
	public String getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "DisableMegafonMSISDN [result=" + result + ", time=" + time + "]";
	}
}
