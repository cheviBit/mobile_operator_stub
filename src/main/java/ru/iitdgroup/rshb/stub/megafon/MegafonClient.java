package ru.iitdgroup.rshb.stub.megafon;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import ru.iitdgroup.rshb.stub.megafon.model.AddPhoneResponse;
import ru.iitdgroup.rshb.stub.megafon.model.ChangeImsi;
import ru.iitdgroup.rshb.stub.megafon.model.InboundType;
import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.Imsi;
import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.ResultChange;

@Component
public class MegafonClient {

	@Value("${megafon.callback.url}")
	private String megafonCallback;

	public void sendChangeImsi(String phone) {
		ChangeImsi changeImsi = new ChangeImsi();
		changeImsi.setType(InboundType.CHANGE_IMSISDN);
		changeImsi.setTime(new Date().toString());
		changeImsi.setResult(new ArrayList<ResultChange>());

		ResultChange resultChange = new ResultChange();
		Imsi imsi = new Imsi();
		imsi.setTime(new Date().toString());
		imsi.setValue(phone);
		resultChange.setImsi(imsi);
		changeImsi.getResult().add(resultChange);

		String plainCreds = "wsUser:wsUser";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64Utils.encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + base64Creds);

		HttpEntity<ChangeImsi> entity = new HttpEntity<ChangeImsi>(changeImsi, headers);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

		ResponseEntity<AddPhoneResponse> exchange = restTemplate.exchange(megafonCallback, HttpMethod.POST, entity, AddPhoneResponse.class);
		AddPhoneResponse postForObject = exchange.getBody();

	}

}
