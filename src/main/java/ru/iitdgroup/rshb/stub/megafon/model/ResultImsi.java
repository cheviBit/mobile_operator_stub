package ru.iitdgroup.rshb.stub.megafon.model;



import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.Imsi;
import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.Status;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "status", "msisdn", "imsi" })
public class ResultImsi {

	@JsonProperty("status")
	private Status status;
	@JsonProperty("msisdn")
	private String msisdn;
	@JsonProperty("imsi")
	private Imsi imsi;

	@JsonProperty("status")
	public Status getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Status status) {
		this.status = status;
	}

	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}

	@JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@JsonProperty("imsi")
	public Imsi getImsi() {
		return imsi;
	}

	@JsonProperty("imsi")
	public void setImsi(Imsi imsi) {
		this.imsi = imsi;
	}

	@Override
	public String toString() {
		return "Result [status=" + status + ", msisdn=" + msisdn + ", imsi=" + imsi + "]";
	}

}
