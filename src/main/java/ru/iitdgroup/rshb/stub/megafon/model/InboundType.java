package ru.iitdgroup.rshb.stub.megafon.model;

public enum InboundType {

	DISABLE, // Оповещение об отключении msisdn
	CHANGE_IMSISDN // Оповещение о новом IMSI

}
