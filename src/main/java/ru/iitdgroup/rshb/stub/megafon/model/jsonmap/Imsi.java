package ru.iitdgroup.rshb.stub.megafon.model.jsonmap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "value", "time" })
public class Imsi {
	@JsonProperty("value")
	public String value;
	@JsonProperty("time")
	public String time;

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("time")
	public String getTime() {
		return time;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@JsonProperty("time")
	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Imsi [value=" + value + ", time=" + time + "]";
	}
}
