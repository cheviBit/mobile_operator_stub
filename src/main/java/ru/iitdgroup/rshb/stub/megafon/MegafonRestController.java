package ru.iitdgroup.rshb.stub.megafon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.iitdgroup.rshb.stub.MobileOperatorStub;
import ru.iitdgroup.rshb.stub.megafon.model.AddPhoneResponse;
import ru.iitdgroup.rshb.stub.megafon.model.PhoneRequest;
import ru.iitdgroup.rshb.stub.megafon.model.ResultImsi;
import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.Imsi;
import ru.iitdgroup.rshb.stub.megafon.model.jsonmap.Status;

@RestController
public class MegafonRestController {

	@Autowired
	MobileOperatorStub checker;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("megafon/imsinotify/v2/msisdns")
	public AddPhoneResponse getResponse(@RequestBody PhoneRequest request) {
		String statusCode = null;

		String msisdn = request.getMsisdn();
		if (checker.isMegafonPhoneAlreadySubscribed(msisdn)) {
			statusCode = MegafonSubcriptionResponceCodeEnum.ALREADY_SUBSCRIBED.getCode();
		} else {
			statusCode = checker.addPhoneInMegafon(msisdn);
		}

		AddPhoneResponse response = new AddPhoneResponse();
		ResultImsi result = new ResultImsi();

		Imsi imsi = new Imsi();
		imsi.setTime("time");
		imsi.setValue("imsi");
		result.setImsi(imsi);

		result.setMsisdn(msisdn);

		Status status = new Status();
		status.setCode(statusCode);
		status.setName(statusCode);
		result.setStatus(status);

		response.setResult(result);

		return response;
	}

}
