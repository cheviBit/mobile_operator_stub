package ru.iitdgroup.rshb.stub.megafon.model.jsonmap;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "msisdn", "imsi" })
public class ResultChange {

	@JsonProperty("msisdn")
	public String msisdn;
	@JsonProperty("imsi")
	public Imsi imsi;

	@JsonProperty("msisdn")
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@JsonProperty("msisdn")
	public String getMsisdn() {
		return msisdn;
	}

	@JsonProperty("imsi")
	public Imsi getImsi() {
		return imsi;
	}

	@JsonProperty("imsi")
	public void setImsi(Imsi imsi) {
		this.imsi = imsi;
	}

	@Override
	public String toString() {
		return "Result [msisdn=" + msisdn + ", imei=" + imsi + "]";
	}
}
