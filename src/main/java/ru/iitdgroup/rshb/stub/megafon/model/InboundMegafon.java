package ru.iitdgroup.rshb.stub.megafon.model;

public abstract class InboundMegafon {

	private InboundType type;

	public InboundType getType() {
		return type;
	}

	public void setType(InboundType type) {
		this.type = type;
	}
}