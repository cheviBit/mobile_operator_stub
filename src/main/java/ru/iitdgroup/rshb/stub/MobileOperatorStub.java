package ru.iitdgroup.rshb.stub;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.xml.datatype.DatatypeConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ru.iitdgroup.rshb.stub.megafon.MegafonClient;
import ru.iitdgroup.rshb.stub.megafon.MegafonSubcriptionResponceCodeEnum;
import ru.iitdgroup.rshb.stub.mtc.MtcClient;
import ru.iitdgroup.rshb.stub.mtc.MtsSubscriptionResponceCodeEnum;
import ru.iitdgroup.rshb.stub.vimpelcom.VimpelComClient;
import ru.iitdgroup.rshb.stub.vimpelcom.VimpelSubcriptionResponceCodeEnum;

@Component
public class MobileOperatorStub {

	private Random rng = new Random();

	@Value("${file.mts.phones}")
	private String fileMtsPhones;

	@Value("${file.megafon.phones}")
	private String fileMegafonPhones;

	@Value("${file.vimpel.phones}")
	private String fileVimpelPhones;

	private String[] names = { fileMtsPhones, fileMegafonPhones, fileVimpelPhones };

	@Autowired
	private MtcClient mtcClient;

	@Autowired
	private VimpelComClient vimpelComClient;

	@Autowired
	private MegafonClient megafonClient;

	public MobileOperatorStub() {
		super();
	}

	public synchronized boolean isSubscribedByAnyOperator(String msisdn) {
		if (isMtsPhoneAlreadySubscribed(msisdn)) {
			return true;
		}
		if (isVimpelPhoneAlreadySubscribed(msisdn)) {
			return true;
		}
		if (isMegafonPhoneAlreadySubscribed(msisdn)) {
			return true;
		}
		return false;
	}

	/**
	 * Добавляем телефон случайному оператору
	 * 
	 * @param msisdn
	 * @return
	 */
	public synchronized String addPhoneInRandomOperator(String msisdn) {

		String code = null;
		try {
			int nextInt = rng.nextInt(names.length);
			String fileName = names[nextInt];
			addToFile(fileName, msisdn);
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}
		code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
		return code;
	}

	/**
	 * Проверяем, обслуживается ли телефон у МТС
	 * 
	 * @param msisdn
	 * @return
	 */
	public synchronized boolean isMtsPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileMtsPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	/**
	 * добавляем телефон к МТС
	 * 
	 * @param msisdn
	 * @return
	 */
	public synchronized String addPhoneInMts(String msisdn) {
		MtsSubscriptionResponceCodeEnum code = null;
		if (isSubscribedByAnyOperator(msisdn)) {
			code = MtsSubscriptionResponceCodeEnum.USER_NOT_FOUND;
		} else {
			try {
				addToFile(fileMtsPhones, msisdn);
				code = MtsSubscriptionResponceCodeEnum.SUCCESS;
			} catch (Exception e) {
				code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR;
			}
		}
		return code.getCode();
	}

	// Удаляем телефон, перезаписываем файл оставшимися телефонами
	public synchronized String deleteMtsPhone(String msisdn) {
		String code = null;

		try {
			StringBuilder newPhones = new StringBuilder();
			List<String> readFromFile = readFromFile(fileMtsPhones);
			Iterator<String> iterator = readFromFile.listIterator();
			while (iterator.hasNext()) {
				String phone = iterator.next();
				if (msisdn.equals(phone)) {
					code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
				} else {
					newPhones.append(phone).append("\n");
				}
			}
			writeToFile(fileMtsPhones, newPhones.toString());
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}

		return code;
	}

	public synchronized boolean isMegafonPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileMegafonPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	// добавляем телефон к Megafon
	public synchronized String addPhoneInMegafon(String msisdn) {
		MegafonSubcriptionResponceCodeEnum code = null;
		if (isSubscribedByAnyOperator(msisdn)) {
			code = MegafonSubcriptionResponceCodeEnum.AGREEMENT_NOT_FOUND;
		} else {
			try {
				addToFile(fileMegafonPhones, msisdn);
				code = MegafonSubcriptionResponceCodeEnum.OK;
			} catch (Exception e) {
				code = MegafonSubcriptionResponceCodeEnum.UNKNOWN_MSISDN;
			}
		}
		return code.getCode();
	}

	public synchronized boolean isVimpelPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileVimpelPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	/**
	 * добавляем телефон к Vimpel
	 * 
	 * @param msisdn
	 * @return
	 */
	public synchronized String addPhoneInVimpel(String msisdn) {
		VimpelSubcriptionResponceCodeEnum code = null;
		if (isSubscribedByAnyOperator(msisdn)) {
			code = VimpelSubcriptionResponceCodeEnum.SUBSCRIBER_NOT_FOUND;
		} else {
			try {
				addToFile(fileVimpelPhones, msisdn);
				code = VimpelSubcriptionResponceCodeEnum.SUCCESS;
			} catch (Exception e) {
				code = VimpelSubcriptionResponceCodeEnum.SYSTEM_NOT_AVAILABLE;
			}
		}
		return code.getCode();
	}

	private synchronized List<String> readFromFile(String fileName) {
		List<String> phones = new ArrayList<String>();
		FileReader reader = null;
		try {
			File f = new File(fileName);
			if (f.exists()) {
				reader = new FileReader(f);
				char[] buffer = new char[(int) f.length()];
				reader.read(buffer);
				String string = new String(buffer);
				String[] split = string.split("\n");
				phones.addAll(Arrays.asList(split));
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
		return phones;
	}

	// Перезаписываем все данные в файле
	private synchronized void writeToFile(String fileName, String msisdn) throws Exception {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.append(msisdn);
			fileWriter.append("\n");
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

	// Добавляем в файл ещё одну строку
	private synchronized void addToFile(String fileName, String msisdn) throws Exception {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName, true);
			fileWriter.append(msisdn);
			fileWriter.append("\n");
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

	public boolean sendInbound(String phone) throws MalformedURLException, DatatypeConfigurationException {
		boolean sent = false;
		if (isMtsPhoneAlreadySubscribed(phone)) {
			mtcClient.sendChangeIMSI(phone);
			sent = true;
		} else if (isVimpelPhoneAlreadySubscribed(phone)) {
			vimpelComClient.sendChangeIMSI(phone);
			sent = true;
		} else if (isMegafonPhoneAlreadySubscribed(phone)) {
			megafonClient.sendChangeImsi(phone);
			sent = true;
		}
		return sent;
	}

}