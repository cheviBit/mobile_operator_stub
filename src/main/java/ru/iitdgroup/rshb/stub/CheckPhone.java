package ru.iitdgroup.rshb.stub;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import ru.iitdgroup.rshb.stub.mtc.MtsSubscriptionResponceCodeEnum;

@Component
public class CheckPhone {

	private Random rng = new Random();

	private static final String fileMtsPhones = "C:/temp/mts.csv";
	private static final String fileMegafonPhones = "C:/temp/megafon.csv";
	private static final String fileVimpelPhones = "C:/temp/vimpel.csv";

	private String[] names = { fileMtsPhones, fileMegafonPhones, fileVimpelPhones };

	public CheckPhone() {
		super();
	}

	public static void main(String[] args) {

		CheckPhone cf = new CheckPhone();

		cf.addPhoneInMts("12345");
		cf.addPhoneInMts("123456");

		cf.isMtsPhoneAlreadySubscribed("2311");
		cf.isMtsPhoneAlreadySubscribed("234");
		cf.isMtsPhoneAlreadySubscribed("566");

		cf.addPhoneInRandomOperator("2311");
		cf.addPhoneInRandomOperator("234");
		cf.addPhoneInRandomOperator("566");
		cf.addPhoneInRandomOperator("2432");
		cf.addPhoneInRandomOperator("756");
		cf.addPhoneInRandomOperator("345");
		cf.addPhoneInRandomOperator("78");
		cf.addPhoneInRandomOperator("345");
		cf.addPhoneInRandomOperator("1345");
		cf.addPhoneInRandomOperator("4345");
		cf.addPhoneInRandomOperator("8345");

		cf.deleteMtsPhone("12345");

	}

	// Добавляем телефон случайному оператору
	public String addPhoneInRandomOperator(String msisdn) {

		String code = null;
		try {
			int nextInt = rng.nextInt(names.length);
			String fileName = names[nextInt];
			addToFile(fileName, msisdn);
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}
		code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
		return code;
	}

	// Проверяем, обслуживается ли телефон у
	// МТС
	public boolean isMtsPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileMtsPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	// добавляем телефон к МТС
	public String addPhoneInMts(String msisdn) {

		String code = null;
		try {
			addToFile(fileMtsPhones, msisdn);
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}
		code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
		return code;
	}

	// Удаляем телефон, перезаписываем файл
	// оставшимися телефонами
	public String deleteMtsPhone(String msisdn) {
		String code = null;

		try {
			StringBuilder newPhones = new StringBuilder();
			List<String> readFromFile = readFromFile(fileMtsPhones);
			Iterator<String> iterator = readFromFile.listIterator();
			while (iterator.hasNext()) {
				String phone = iterator.next();
				if (msisdn.equals(phone)) {
					code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
				} else {
					newPhones.append(phone).append("\n");
				}
			}
			writeToFile(fileMtsPhones, newPhones.toString());
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}

		return code;
	}

	public boolean isMegafonPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileMegafonPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	// добавляем телефон к Megafon
	public String addPhoneInMegafon(String msisdn) {

		String code = null;
		try {
			addToFile(fileMegafonPhones, msisdn);
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}
		code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
		return code;
	}

	public boolean isVimpelPhoneAlreadySubscribed(String msisdn) {
		boolean code = false;
		List<String> readFromFile = readFromFile(fileVimpelPhones);
		for (String phone : readFromFile) {
			if (msisdn.equals(phone)) {
				code = true;
			}
		}
		return code;
	}

	// добавляем телефон к Vimpel
	public String addPhoneInVimpel(String msisdn) {

		String code = null;
		try {
			addToFile(fileVimpelPhones, msisdn);
		} catch (Exception e) {
			code = MtsSubscriptionResponceCodeEnum.PROCESS_ERROR.getCode();
		}
		code = MtsSubscriptionResponceCodeEnum.SUCCESS.getCode();
		return code;
	}

	private static List<String> readFromFile(String fileName) {
		List<String> phones = null;
		FileReader reader = null;
		try {
			File f = new File(fileName);
			if (f.exists()) {
				reader = new FileReader(f);
				char[] buffer = new char[(int) f.length()];
				reader.read(buffer);
				String string = new String(buffer);
				String[] split = string.split("\n");
				phones = Arrays.asList(split);
			}
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {

				reader.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
		return phones;
	}

	// Перезаписываем все данные в файле
	private void writeToFile(String fileName, String msisdn) throws Exception {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.append(msisdn);
			fileWriter.append("\n");
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

	// Добавляем в файл ещё одну строку
	private void addToFile(String fileName, String msisdn) throws Exception {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName, true);
			fileWriter.append(msisdn);
			fileWriter.append("\n");
		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
			throw new Exception(e);
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}
	}

}
