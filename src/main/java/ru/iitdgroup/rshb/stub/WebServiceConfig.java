package ru.iitdgroup.rshb.stub;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import ru.iitdgroup.rshb.stub.mtc.MtcEndpoint;
import ru.iitdgroup.rshb.stub.vimpelcom.VimpelComEndpoint;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/ws/*");
	}

	@Bean(name = "mtc_service")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema mtcSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("MtsPort");
		wsdl11Definition.setLocationUri("/mtc_service");
		wsdl11Definition.setTargetNamespace(MtcEndpoint.NAMESPACE_URI);
		wsdl11Definition.setSchema(mtcSchema);
		return wsdl11Definition;
	}

	@Bean(name = "vimpel_service")
	public DefaultWsdl11Definition defaultVimpelDefinition(XsdSchema vimpelSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("VimpelPort");
		wsdl11Definition.setLocationUri("/vimpel_service");
		wsdl11Definition.setTargetNamespace(VimpelComEndpoint.NAMESPACE_URI);
		wsdl11Definition.setSchema(vimpelSchema);
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema mtcSchema() {
		return new SimpleXsdSchema(new ClassPathResource("META-INF/wsdl/mtc_out.xsd"));
	}

	@Bean
	public XsdSchema vimpelSchema() {
		return new SimpleXsdSchema(new ClassPathResource("META-INF/wsdl/OnlineNotificationPartners_OnlineNotificationPartners.xsd"));
	}

}
