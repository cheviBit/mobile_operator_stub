package ru.iitdgroup.rshb.stub.mtc;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import org.springframework.stereotype.Component;

import server_url.ExternalServer;
import server_url.ExternalServerSoap;

@Component
public class MtcClient {

	private static final String SERVICE_URL = "http://localhost:8080/rshb/api/webservices/mts-ws-service?wsdl";

	ExternalServer service;

	public void sendChangeIMSI(String phone) throws DatatypeConfigurationException, MalformedURLException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date(System.currentTimeMillis()));
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		java.net.Authenticator myAuth = new java.net.Authenticator() {
			@Override
			protected java.net.PasswordAuthentication getPasswordAuthentication() {
				return new java.net.PasswordAuthentication("wsUser", "wsUser".toCharArray());
			}
		};
		java.net.Authenticator.setDefault(myAuth);

		service = new ExternalServer();

		ExternalServerSoap port = service.getExternalServerSoap();
		Map<String, Object> context = ((BindingProvider) port).getRequestContext();
		context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, SERVICE_URL);

		BindingProvider prov = (BindingProvider) port;
		prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "wsUser");
		prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "wsUser");
		port.changeId(phone, "IMSI", date2);
	}

}
