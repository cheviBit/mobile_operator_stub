package ru.iitdgroup.rshb.stub.mtc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ru.iitdgroup.rshb.stub.MobileOperatorStub;
import _91._35._241._10._8082.soap.profilemanage.AddProfile;
import _91._35._241._10._8082.soap.profilemanage.AddProfileResponse;
import _91._35._241._10._8082.soap.profilemanage.DeleteProfile;
import _91._35._241._10._8082.soap.profilemanage.DeleteProfileResponse;
import _91._35._241._10._8082.soap.profilemanage.ResultSet;

@Endpoint
public class MtcEndpoint {

	@Autowired
	MobileOperatorStub checker;

	public static final String NAMESPACE_URI = "http://10.241.35.91:8082/soap/profilemanage";

	public MtcEndpoint() {
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddProfile")
	@ResponsePayload
	public AddProfileResponse addProfile(@RequestPayload AddProfile request) {
		try {
			String msisdn = request.getMSISDN();
			String statusCode = null;

			if (checker.isMtsPhoneAlreadySubscribed(msisdn)) {
				statusCode = MtsSubscriptionResponceCodeEnum.ALREADY_SUBSCRIBED.getCode();
			} else {
				statusCode = checker.addPhoneInMts(msisdn);
			}

			AddProfileResponse response = new AddProfileResponse();

			ResultSet resultSet = new ResultSet();
			resultSet.setStatusCode(statusCode);
			response.setReturn(resultSet);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DeleteProfile")
	@ResponsePayload
	public DeleteProfileResponse deleteProfile(@RequestPayload DeleteProfile request) {
		try {
			String msisdn = request.getMSISDN();
			String statusCode = null;

			if (!checker.isMtsPhoneAlreadySubscribed(msisdn)) {
				statusCode = MtsSubscriptionResponceCodeEnum.NOT_SUBSCRIBED.getCode();
			} else {
				statusCode = checker.deleteMtsPhone(msisdn);
			}
			DeleteProfileResponse response = new DeleteProfileResponse();
			ResultSet resultSet = new ResultSet();
			resultSet.setStatusCode(statusCode);
			response.setReturn(resultSet);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}