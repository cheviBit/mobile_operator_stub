package ru.iitdgroup.rshb.stub.mtc;

public enum MtsSubscriptionResponceCodeEnum {

	SUCCESS("0000", "Success", "Заявка успешно зарегистрирована"), //
	NOT_SUBSCRIBED("1000", "Not Subscribed", "Абонент не найден (при отключении услуги)"), //
	ALREADY_SUBSCRIBED("1001", "Already subscribed", "Абонент уже подключен к услуге (при подключении)"), //
	PROCESS_ERROR("1002", "ProcessError", "Неизвестная ошибка"), //
	OPEN_APPLICATION("1003", "Open application", "У абонента существует открытая заявка"), //
	ABSENT_SUBSCRIBER("1004", "Absent subscriber", "Абонентский номер не обслуживается МТС"), //
	USER_NOT_FOUND("2001", "User Not Found", "Неверный логин/пароль"), //
	CONNECTION_FAILURE("3001", "Connection failure", "Ошибка подключения");

	private String code;

	private String name;

	private String comment;

	private MtsSubscriptionResponceCodeEnum(String code, String name, String comment) {
		this.code = code;
		this.name = name;
		this.comment = comment;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getComment() {
		return comment;
	}

	public static MtsSubscriptionResponceCodeEnum getCode(String responceStatus) {
		for (MtsSubscriptionResponceCodeEnum codeEnum : values()) {
			if (codeEnum.getCode().equals(responceStatus)) {
				return codeEnum;
			}
		}
		return null;
	}

}
