package ru.iitdgroup.rshb.stub.vimpelcom;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import on.partners_interface.OnlineNotificationPartnersClient;
import on.partners_interface.OnlineNotificationPartnersClientService;
import on.partners_interface.schema.GetSubscriberEventRequest;
import on.partners_interface.schema.SubscriberEvent;

import org.springframework.stereotype.Component;

@Component
public class VimpelComClient {

	private static final String SERVICE_URL = "http://localhost:8080/rshb/api/webservices/vimpelcom-ws-service?wsdl";

	OnlineNotificationPartnersClientService service;

	public void sendChangeIMSI(String phone) throws DatatypeConfigurationException, MalformedURLException {
		java.net.Authenticator myAuth = new java.net.Authenticator() {
			@Override
			protected java.net.PasswordAuthentication getPasswordAuthentication() {
				return new java.net.PasswordAuthentication("wsUser", "wsUser".toCharArray());
			}
		};
		java.net.Authenticator.setDefault(myAuth);

		service = new OnlineNotificationPartnersClientService();

		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date(System.currentTimeMillis()));
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

		GetSubscriberEventRequest getSubscriberEventRequest = new GetSubscriberEventRequest();
		getSubscriberEventRequest.setLogin("wsUser");
		getSubscriberEventRequest.setPwd("wsUser");
		SubscriberEvent subscriberEvent = new SubscriberEvent();
		subscriberEvent.setDate(date2);
		subscriberEvent.setMsisdn(phone);
		subscriberEvent.setReason("сhangeIMSI");
		getSubscriberEventRequest.setEvent(subscriberEvent);

		OnlineNotificationPartnersClient port = service.getOnlineNotificationPartnersClientPort();
		Map<String, Object> context = ((BindingProvider) port).getRequestContext();
		context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, SERVICE_URL);
		
		BindingProvider prov = (BindingProvider) port;
		prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "wsUser");
		prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "wsUser");
		port.getSubscriberEvent(getSubscriberEventRequest);
	}

}
