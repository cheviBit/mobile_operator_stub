package ru.iitdgroup.rshb.stub.vimpelcom;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import on.partners_interface.SubscriptionManagementFault;
import on.partners_interface.schema.AddSubscriptionRequest;
import on.partners_interface.schema.AddSubscriptionResponse;
import on.partners_interface.schema.RemoveSubscriptionRequest;
import on.partners_interface.schema.RemoveSubscriptionResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ru.iitdgroup.rshb.stub.MobileOperatorStub;

@Endpoint
public class VimpelComEndpoint {

	@Autowired
	MobileOperatorStub checker;

	public static final String NAMESPACE_URI = "urn:on:partners-interface:schema";

	public VimpelComEndpoint() {
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addSubscriptionRequest")
	@ResponsePayload
	public JAXBElement<AddSubscriptionResponse> addSubscription(@RequestPayload JAXBElement<AddSubscriptionRequest> request)
			throws SubscriptionManagementFault {
		String msisdn = request.getValue().getMsisdn();
		String statusCode = null;
		
		if (checker.isVimpelPhoneAlreadySubscribed(msisdn)) {
			statusCode = VimpelSubcriptionResponceCodeEnum.SUCCESS.getCode();
		} else {
			statusCode = checker.addPhoneInVimpel(msisdn);
		}

		AddSubscriptionResponse response = new AddSubscriptionResponse();
		response.setResult(statusCode);

		return new JAXBElement<AddSubscriptionResponse>(new QName(NAMESPACE_URI, "addSubscriptionRequest"), AddSubscriptionResponse.class, response);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeSubscriptionRequest")
	@ResponsePayload
	public JAXBElement<RemoveSubscriptionResponse> removeSubscription(@RequestPayload JAXBElement<RemoveSubscriptionRequest> request) {
		RemoveSubscriptionResponse response = new RemoveSubscriptionResponse();
		return new JAXBElement<RemoveSubscriptionResponse>(new QName(NAMESPACE_URI, "RemoveSubscriptionRequest"), RemoveSubscriptionResponse.class,
				response);
	}

}
