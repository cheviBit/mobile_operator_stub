package ru.iitdgroup.rshb.stub.vimpelcom;

public enum VimpelSubcriptionResponceCodeEnum {

	SUCCESS("SUCCESS", "Подписан"), INVALID_DATA("INVALID_DATA", "Запрос не соответствует спецификации"), //
	SYSTEM_NOT_AVAILABLE("SYSTEM_NOT_AVAILABLE", "Система временно не доступна"), //
	AUTENTIFICATION("AUTENTIFICATION", "Логин/пароль не верны"), //
	SUBSCRIBER_NOT_FOUND("SUBSCRIBER_NOT_FOUND", "Абонент не найден"), //
	SUBSCRIBER_IS_SUSPEND("SUBSCRIBER_IS_SUSPEND", "Абонент блокирован"), //
	SUBSCRIBER_IS_CANCEL("SUBSCRIBER_IS_CANCEL", "Контракт с абонентом расторгнут"), //
	RESTRICT_FOR_SUBSCRIBER("RESTRICT_FOR_SUBSCRIBER", "Подписка на события по абоненту запрещены"), //
	LIMIT_SUBSCRIBERS("LIMIT_SUBSCRIBERS", "превышено количество номеров для подписки"), //
	;

	private String code;

	private String comment;

	private VimpelSubcriptionResponceCodeEnum(String code, String comment) {
		this.code = code;
		this.comment = comment;
	}

	public String getCode() {
		return code;
	}

	public String getComment() {
		return comment;
	}

	public static VimpelSubcriptionResponceCodeEnum getByCode(String result) {
		for (VimpelSubcriptionResponceCodeEnum codeEnum : values()) {
			if (codeEnum.getCode().equals(result)) {
				return codeEnum;
			}
		}
		return null;
	}
}
